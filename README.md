# Colorbox simple load

Colorbox Simple Load allows the user to open url within a colorbox.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/colorbox_simple_load).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/colorbox_simple_load).


## Contents of this file

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Colorbox](https://drupal.org/project/colorbox)


## Installation

Install the Colorbox Simple Load module as you would normally install a 
contributed Drupal module.
Visit [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules) for further information.


## Configuration

To create an element which opens the colorbox on select:

Add the class colorbox-load to an element for the content you wish to open.
Eg, `<a class="colorbox-load" href="url">View</a>`.


## Maintainers

- Tamas Pinter - [mr.york](https://www.drupal.org/u/mryork)
- Bálint Nagy - [nagy.balint](https://www.drupal.org/u/nagybalint)

**Supporting maintenance and support provided by:**

- [Agence Inovae](https://www.drupal.org/agence-inovae)
